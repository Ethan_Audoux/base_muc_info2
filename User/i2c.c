#include "i2c.h"
#include "bluetooth.h"
#include "touch\lcd_api.h"
#include "touch\ili_lcd_general.h"


const int max_buff_i2c = 50;


/*
Initialisation du module I2C
*/
void InitI2C() {
	
	uint32_t clockrate = 1e6; // clockrate donnee dans la documentation
	
	I2C_Init(LPC_I2C0, clockrate);

	I2C_Cmd(LPC_I2C0, ENABLE); // Active le module
	
	reset_curseur(); // Reset l'emplacement des curseur de lecture

}

/*
Fonction permettant l'ecriture de data d'une longueur length
a l'emplacement 'emplacement' sur la page 'page'
Envoie:
[adresse_escl][page][0][emplacement][data]
*/
void I2C_write(uint8_t * data, uint8_t length, uint8_t emplacement, uint8_t page){
	int i;
	uint32_t adresse_escl;
	I2C_M_SETUP_Type configI2C;

	uint8_t data2[max_buff_i2c + 1];
	
	data2[0] = emplacement; // on ecrit l'emplacement au debut de la data
	
	// on ecrit la data a la suite de l'emplacement
	for (i=0; i<length+1; i++) {
		data2[i+1] = data[i];
	}
	
	// On initialise l'adresse de l'esclave suivi de la memoire et on decale le tt de 1
	adresse_escl = set_adresse(page) >> 1;

	// Configuration de l'ecriture
	configI2C.sl_addr7bit = adresse_escl;
	configI2C.tx_data = data2;
	configI2C.tx_length = length+1;
	configI2C.tx_count = 0;
	 
	configI2C.rx_data = NULL;
	configI2C.rx_length = 0;
	configI2C.rx_count = 0;

	configI2C.retransmissions_count = 0;
	configI2C.retransmissions_max = 1;
	
	I2C_MasterTransferData(LPC_I2C0, &configI2C, I2C_TRANSFER_POLLING); // Fonction de transfert de l'I2C
}

/*
Fonction permettant la lecture de data d'une longueur length
a l'emplacement 'emplacement' sur la page 'page'
Reception
[adresse_escl][page][0][emplacement][adresse_escl][page][1][data]
*/
void I2C_read(uint8_t * data, uint8_t taille, int empla, int page){
	I2C_M_SETUP_Type configI2C;
	uint32_t adresse_escl;
	uint8_t empla_uint = empla;
		
	// On initialise l'adresse de l'esclave suivi de la memoire et on decale le tt de 1
	adresse_escl = set_adresse(page) >> 1;

	configI2C.sl_addr7bit = adresse_escl;
	
	// Ecriture de rien a l'emplacement voulu pour deplacer le curseur interne de la memoire
	configI2C.tx_data = &empla_uint;
	configI2C.tx_length = 1;
	configI2C.tx_count = 0;
	
	// Configuration de la lecture
	configI2C.rx_data = data;
	configI2C.rx_length = taille;
	configI2C.rx_count = 0;

	configI2C.retransmissions_count = 0;
	configI2C.retransmissions_max = 1;
	
	I2C_MasterTransferData(LPC_I2C0, &configI2C, I2C_TRANSFER_POLLING); // Fonction permettant le transfert
}

/*
lire l'emplacement du curseur d'ecriture
*/
uint8_t I2C_read_empla_cur_wri() {
	
	uint8_t data;
	
	I2C_read(&data, 1, 0, 0);
	
	return data;	
}

/*
lire la page du curseur d'ecriture
*/
uint8_t I2C_read_page_cur_wri() {
	
	uint8_t data;
	
	I2C_read(&data, 1, 1, 0);
	
	return data;	
}

/*
lire l'emplacement du curseur de lecture pour la musique
*/
uint8_t I2C_read_empla_cur_rea_mus() {
	
	uint8_t data;
	
	I2C_read(&data, 1, 2, 0);
	
	return data;	
}

/*
lire la page du curseur de lecture pour la musqiue
*/
uint8_t I2C_read_page_cur_rea_mus() {
	
	uint8_t data;
	
	I2C_read(&data, 1, 3, 0);
	
	return data;	
}

/*
lire l'emplacement du curseur de lecture pour les touches
*/
uint8_t I2C_read_empla_cur_rea_tou() {
	
	uint8_t data;
		
	I2C_read(&data, 1, 4, 0);

	return data;	
}

/*
lecture de la page du curseur de lecture pour les touches
*/
uint8_t I2C_read_page_cur_rea_tou() {
	
	uint8_t data;
	
	I2C_read(&data, 1, 5, 0);
	
	return data;	
}

/*
lecture du tempo
*/
uint16_t I2C_read_tempo() {
	
	uint8_t data[3];
	char datac[3];
	int i;
	
	I2C_read(data, 3, 10, 0);
	
	for (i=0; i<3; i++){
		datac[i] = data[i];
	}
	
	return set_tempo(datac);
}

/*
ecriture de l'emplacement du curseur d'ecriture
*/
void I2C_write_empla_cur_wri(uint8_t empla){
	
	I2C_write(&empla, 1, 0, 0);
}

/*
ecriture de l'emplacement de la page du curseur d'ecriture
*/
void I2C_write_page_cur_wri(uint8_t page){
	
	I2C_write(&page, 1, 1, 0);
}

/*
ecriture de l'emplacement du curseur de lecture pour la musique
*/
void I2C_write_empla_cur_rea_mus(uint8_t empla){
	
	I2C_write(&empla, 1, 2, 0);
}

/*
ecriture de la page du curseur de lecture pour la musique
*/
void I2C_write_page_cur_rea_mus(uint8_t page){
	
	I2C_write(&page, 1, 3, 0);
}

/*
ecriture de l'emplacement du curseur de lecture pour les touches
*/
void I2C_write_empla_cur_rea_tou(uint8_t empla){
	
	I2C_write(&empla, 1, 4, 0);
}

/*
ecriture de la page du curseur de lecture pour les touches
*/
void I2C_write_page_cur_rea_tou(uint8_t page){
	
	I2C_write(&page, 1, 5, 0);
}

/*
ecriture du tempo
*/
void I2C_write_tempo(uint8_t * tempo) {
	
	I2C_write(tempo, 3, 10, 0);
}

/*
Fonction global permettant de split la chaine de caractere en chaine
de 45 pour l'ecrire en memoire
*/
void I2C_write_mus_glob(char * str, int len) {
	int i = 0;
	int compteur = 0;
	char str2[max_buff_i2c];
	while (i < len) {
		str2[compteur] = str[i];
		i++;
		compteur++;
		if (compteur == max_buff_i2c) {
			I2C_write_mus(str2, compteur);
			compteur = 0;
		}
	}
	I2C_write_mus(str2, compteur);
}

/*
Ecriture de la musique en memoire
*/
void I2C_write_mus(char * chaine, int taille) {
	int i;
	int empla;
	uint8_t empla_uint;
	uint8_t page;
	uint8_t next_page;
	uint8_t chaine_res[max_buff_i2c+1];
	uint8_t tempo[3];
	
	if (chaine[0] == 'z') { // Si c'est le debut de la musique
		empla_uint = 0;		// On ecrit au debut de la page 1 
		page = 1;
		
		
		for (i=0; i<3; i++) {
			tempo[i] = chaine[i+1];
		}
		
		I2C_write_tempo(tempo); // ecriture du tempo
		
		reset_curseur(); // On reset les curseur de lecture
		
		// On met le reste de la musique dans une nouvelle chaine avec le bon type
		for (i=4; i<taille; i++) {
			chaine_res[i] = chaine[i];
		}
		
		I2C_write(&(chaine_res[4]), taille-4, empla_uint, page);
		
		empla = page*256 + empla_uint + taille-4; // On actualise le nouvel emplacement du curseur d'ecriture
		
		next_page = know_page(empla); // On cherche a quelle page ecrire en fonction du nouvel emplacement
		
		// On ecrit en memoire le nouvel emplacement et la nouvelle page du curseur d'ecriture
		I2C_write_empla_cur_wri(empla%256);
		I2C_write_page_cur_wri(next_page);
		
	}
	// On recoit la suite de la musique precedemment envoye 
	else {
		// On recupere l'emplacement du curseur d'ecriture
		empla_uint = I2C_read_empla_cur_wri();
		page = I2C_read_page_cur_wri();
		
		// On met la data a ecrire dans une nouvelle chaine avec le bon type
		for (i=0; i<taille; i++) {
			chaine_res[i] = chaine[i];
		}
		
		I2C_write(chaine_res, taille, empla_uint, page); // On ecrit la musique a l'emplacement voulu

		empla = page*256 + empla_uint + taille; /// On actualise le nouvel emplacement du curseur d'ecriture
		
		next_page = know_page(empla); // On cherche la nouvelle page en fonction du nouvel emplacement
				
		// On ecrit en memoire le nouvel emplacement et la nouvelle page du curseur d'ecriture
		I2C_write_empla_cur_wri(empla%256);
		I2C_write_page_cur_wri(next_page);
	}		
}


		
	
/*
Lecture de la musique en memoire
*/
void I2C_read_mus(char * data, int mus_tou) {
	
	uint8_t data_uint[2];
	
	int empla;
	
	uint8_t empla_uint;
	uint8_t page;
	
	// On regarde si c'est la fonction d'affichage ou la fonction du son qui appel
	// la fonction et on recupere le curseur associe 
	if (mus_tou == 0) {
		empla_uint = I2C_read_empla_cur_rea_mus();
		page = I2C_read_page_cur_rea_mus();
	}
	else if (mus_tou == 1) {
		empla_uint = I2C_read_empla_cur_rea_tou();
		page = I2C_read_page_cur_rea_tou();
	}
	
	I2C_read(data_uint, 2, empla_uint, page); // On lit les 2 prochains caractere (la prochaine note) dans la memoire a l'emplacement voulu
	
	empla = empla_uint + 2; // On incremente de 2 l'emplacement 
	
	// On verifie si on change de page en incrementant l'emplacement
	if (empla > 255) {
		empla_uint = empla - 256;
		page = page + 1;
	}
	else {
		empla_uint = empla;
	}
	
	// On ecrit dans la memoire l'emplacement du curseur de lecture utilise au debut
	if (mus_tou == 0) {
		I2C_write_empla_cur_rea_mus(empla_uint);
		I2C_write_page_cur_rea_mus(page);
	}
	else if (mus_tou == 1) {
		I2C_write_empla_cur_rea_tou(empla_uint);
		I2C_write_page_cur_rea_tou(page);
	}

	// On place la data dans le tableau fourni en entree de la fonction
	data[0] = data_uint[0];
	data[1] = data_uint[1];
	

	
	
	
}

/*
On remet au debut de la musique les 2 curseur de lecture
*/
void reset_curseur() {
	
	I2C_write_empla_cur_rea_mus(0);
	I2C_write_page_cur_rea_mus(1);

	I2C_write_empla_cur_rea_tou(0);
	I2C_write_page_cur_rea_tou(1);
	
}


/*
Retourne la page en fonction de l'emplacement depuis le debut de la memoire
*/
uint8_t know_page(int empla){
		uint8_t res = empla/256;
	
	return res;
}

/*
Renvoie l'adresse de l'esclave en fonction de la page a laquelle on souhaite effectuer une action
*/
uint32_t set_adresse(int page) {
	//uint8_t page_uint = page;
	
	uint32_t res;
	
	if (page == 0) {
			res = mainID;
	} else if (page == 1) {
			res = mainID | 0x02;
	} else if (page == 2) {
			res = mainID | 0x04;
	} else if (page == 3) {
			res = mainID | 0x06;
	} else if (page == 4) {
			res = mainID | 0x08;
	} else if (page == 5) {
			res = mainID | 0x0A;
	} else if (page == 6) {
			res = mainID | 0x0C;
	} else if (page == 7) {
			res = mainID | 0x0D;
	} else{
			res = mainID;
	};
	

	
	return res;
}

/*
Renvoie un uint16_t correspondant au tempo de la musique a partir d'un tableau de 3 caracteres
*/
uint16_t set_tempo(char * data) {
	
	uint16_t cent = data[0]-48;
	uint16_t dix = data[1]-48;
	uint16_t uni = data[2]-48;
	
	return cent*100 + dix*10 + uni;	
}



