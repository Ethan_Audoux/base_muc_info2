#include "LPC17xx.h"
#include "son.h"
#include "LPC17xx_timer.h"
#include "constantes.h"
//#include "globaldec.h"
#include <stdio.h>
#include "i2c.h"
#define musique_loop FALSE

// char musique[300] ; 
// int lectMusique = 0 ; 
char buffMusique[3] ; 
char currNote[3] ; 

void init_musique(){
	int i ; 
	for(i = 0; i<3; i++){currNote[i] = '\0' ; } // la première note n'existe pas
	
}

int strcmp2(char * c1, char * c2){
	return (c1[0] == c2[0] && c1[1] == c2[1]); 
}

void timer1_init(){ // ce timer est utilis� pour g�n�rer les notes
	// structures de configuration
	TIM_TIMERCFG_Type T1 ; 
	TIM_MATCHCFG_Type M0 ; 
	
	T1.PrescaleOption = TIM_PRESCALE_USVAL ; // prescale en �s
	T1.PrescaleValue  = 1 ; // pr�cision de 1�s
	
	M0.MatchChannel = 0 ; 			// on utilise MR0
	M0.IntOnMatch = SET ; 	  // on d�clenche une IT sur le match
	M0.StopOnMatch = DISABLE ; 	// on s'arr�te pas au premier match
	M0.ResetOnMatch = SET ;  // on reset le timer � chaque match 
	M0.MatchValue = (int)(1000000000/(4*440)) ; 	// on match sur 1/(2f/10000) car c'est des demie periode
	M0.ExtMatchOutputType = 0; // on fait rien si en cas de match externe
	
	TIM_Init(LPC_TIM1, TIM_TIMER_MODE,&(T1)) ; 
	TIM_ConfigMatch(LPC_TIM1, &M0) ;
}


void t1_note(long f){ // on supposera que f est dans la bonne unite
	if(f > 0){
		long T =  (long)(1000000000000/(2*f)) ; 
		TIM_Cmd(LPC_TIM1, ENABLE) ; // on demarre le timer si besoin
		TIM_UpdateMatchValue(LPC_TIM1, 0,T ); 
		TIM_ResetCounter(LPC_TIM1); 
	}
	else{
		TIM_Cmd(LPC_TIM1, DISABLE); 
	}
}
void timer0_init(){ // ce timer est utilis� pour s'occuper du bpm
	// structures de configuration
	TIM_TIMERCFG_Type T0 ; 
	TIM_MATCHCFG_Type M0 ; 
	
	T0.PrescaleOption = TIM_PRESCALE_USVAL ; // prescale en �s
	T0.PrescaleValue  = 1 ; // pr�cision de 1�s
	
	M0.MatchChannel = 0 ; 			// on utilise MR0
	M0.IntOnMatch = SET ; 	  // on d�clenche une IT sur le match
	M0.StopOnMatch = DISABLE ; 	// on s'arr�te pas au premier match
	M0.ResetOnMatch = SET ;  // on reset le timer � chaque match 
	M0.MatchValue = (500) ; 	// inutile car remis � jour dans la fonction pour 
	M0.ExtMatchOutputType = 0; // on fait rien si en cas de match externe
	
	TIM_Init(LPC_TIM0, TIM_TIMER_MODE,&(T0)) ; 
	TIM_ConfigMatch(LPC_TIM0, &M0) ;
}

void t0_bpm(int bpm){ // bpm -> battements par minutes 
	if(bpm > 0){
		TIM_Cmd(LPC_TIM0, ENABLE) ; // on demarre le timer si besoin
		TIM_UpdateMatchValue(LPC_TIM0, 0, (int)(60000000/bpm)); 
	}
	else{
		TIM_Cmd(LPC_TIM0, DISABLE); 
	}
}

void TIMER1_IRQHandler(){ 
	if ((LPC_GPIO1->FIOPIN)>>9 & 0x0001){
		LPC_GPIO1->FIOPIN &= ~(1<<9) ; // on met la sortie � 0	
	}
	else {
		LPC_GPIO1->FIOPIN |= (1<<9) ; // on met la sortie � 1
	}
	TIM_ClearIntPending(LPC_TIM1, TIM_MR0_INT);
}
void TIMER0_IRQHandler(){
	I2C_read_mus(buffMusique, 0); 
			if(!strcmp2(buffMusique, currNote)){
				currNote[0] = buffMusique[0] ; 
				currNote[1] = buffMusique[1] ;
				switch(buffMusique[1]){
					case '2' : 
						switch(buffMusique[0]){
							case 'c' : t1_note(note_c2);break ; 
							case 'i' : t1_note(note_i2);break ;
							case 'd' : t1_note(note_d2);break ;
							case 'j' : t1_note(note_j2);break ;
							case 'e' : t1_note(note_e2);break ;
							case 'f' : t1_note(note_f2);break ;
							case 'k' : t1_note(note_k2);break ;
							case 'g' : t1_note(note_g2);break ;
							case 'l' : t1_note(note_l2);break ;
							case 'a' : t1_note(note_a2);break ;
							case 'h' : t1_note(note_h2);break ;
							case 'b' : t1_note(note_b2);break ;
						}
						break ; 
					case '3' : 
						switch(buffMusique[0]){
						 case 'c' : t1_note(note_c3);break ;
						 case 'i' : t1_note(note_i3);break ;
						 case 'd' : t1_note(note_d3);break ;
						 case 'j' : t1_note(note_j3);break ;
						 case 'e' : t1_note(note_e3);break ;
						 case 'f' : t1_note(note_f3);break ;
						 case 'k' : t1_note(note_k3);break ;
						 case 'g' : t1_note(note_g3);break ;
						 case 'l' : t1_note(note_l3);break ;
						 case 'a' : t1_note(note_a3);break ;
						 case 'h' : t1_note(note_h3);break ;
						 case 'b' : t1_note(note_b3);break ;
						}
						break ; 
					case '4' : 
						switch(buffMusique[0]){
						 case 'c' : t1_note(note_c4);break ;
						 case 'i' : t1_note(note_i4);break ;
						 case 'd' : t1_note(note_d4);break ;
						 case 'j' : t1_note(note_j4);break ;
						 case 'e' : t1_note(note_e4);break ;
						 case 'f' : t1_note(note_f4);break ;
						 case 'k' : t1_note(note_k4);break ;
						 case 'g' : t1_note(note_g4);break ;
						 case 'l' : t1_note(note_l4);break ;
						 case 'a' : t1_note(note_a4);break ;
						 case 'h' : t1_note(note_h4);break ;
						 case 'b' : t1_note(note_b4);break ;
						}
						break ; 
					case '5' : t1_note(note_c5) ; break ; // note la plus aigue faisable
					case 'y' : t1_note(0) ; t0_bpm(0) ; break ;
					case ' ' : t1_note(0) ; break ;  
				}
				TIM_ClearIntPending(LPC_TIM0, TIM_MR0_INT);
			}
			else TIM_ClearIntPending(LPC_TIM0, TIM_MR0_INT);	 
}
	
