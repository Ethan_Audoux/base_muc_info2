#ifndef JEU_H
#define JEU_H

void nextArrow(void);
void initGame(void);
void startGame(void);
void initArrowList(void);
void checkJoyInput(void);
void checkNextFrame(void);
int noteToDirection(char c);
int jeuEnCours(void);
void resetGame(void);

#endif
