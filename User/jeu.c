#include <stdio.h>
#include "jeu.h"
#include "globaldec.h"
#include "touch/ili_lcd_general.h"
#include "lpc17xx_timer.h"
#include "affichagelcd.h"
#include "son.h"
#include "touch\lcd_api.h"
#include "i2c.h"
#include "analogJoystick.h"
#include "Bluetooth.h"

uint8_t prevJoyPos = CENTER;

int score = 0;
char scoreString[15];

//char music[300];

arrow arrowList[10];

uint8_t joyPos = CENTER;

uint8_t nextFrameFlag = 0;
uint8_t joyMoved = 0;

uint8_t musicStarted = 0;

uint16_t indiceMusique = 0;
char noteEnCours[2];

uint8_t finFleches = 0;
uint8_t flagJeuEnCours = 0;

uint16_t bpm;	//bpm pour �viter un d�calage notes/fl�che : 150, 180, 225, 300, 450, 900
uint8_t arrowSpeed;	//Vitesse des fl�ches, en pixels/frame
uint16_t distanceBetweenArrows;	//Distance en pixels s�parant deux fl�ches cons�cutives

void nextArrow() {
	uint8_t i;
	effacer_fleche(arrowList[0].yPos,arrowList[0].direction); //On efface la premi�re fl�che de l'�cran avant de la supprimer
	for(i = 0; i < 9; i++) {
		arrowList[i] = arrowList[i+1]; // on supprime la premi�re fl�che et on d�cale les autres
	}
	I2C_read_mus(noteEnCours, 1);
	//noteEnCours[0] = music[indiceMusique];
	//noteEnCours[1] = music[indiceMusique+1];
	//indiceMusique += 2;
	arrowList[9].yPos = arrowList[8].yPos - distanceBetweenArrows;	//On ajoute la prochaine fl�che au bout du tableau
	if (finFleches) {
		arrowList[9].direction = END;
	} else {
		arrowList[9].direction = noteToDirection(noteEnCours[0]);
	}
	if (noteEnCours[0] == 'y') {
		finFleches = 1;
	} 
}

	void TIMER2_IRQHandler(){
		nextFrameFlag = 1; //Drapeau lev� pour afficher la prochaine frame
		
		prevJoyPos = joyPos;	//On conserve la position pr�c�dente du joystick
		joyPos = lectureDirection();
		
		if(joyPos != prevJoyPos) joyMoved = 1;	//On l�ve le drapeau indiquant que le joystick a boug�, le cas �ch�ant
		TIM_ClearIntPending(LPC_TIM2, TIM_MR0_INT);
	}
	
	void initGame(){
		
		//Initialisation du timer g�rant le rafra�chissement de l'�cran
		
		TIM_TIMERCFG_Type cfg;
		TIM_MATCHCFG_Type matchCfg;
		
		cfg.PrescaleOption = TIM_PRESCALE_USVAL;
		cfg.PrescaleValue = 1;	// pr�cision de 1 �s
		TIM_Init(LPC_TIM2, TIM_TIMER_MODE, &cfg);
		
		matchCfg.MatchChannel = 0;
		matchCfg.IntOnMatch = ENABLE;	// interrupt on match
		matchCfg.StopOnMatch = DISABLE;
		matchCfg.ResetOnMatch = ENABLE;	// reset on match
		matchCfg.ExtMatchOutputType = 0;
		matchCfg.MatchValue = 66666;	//	dur�e du timer : 1/15 s
		TIM_ConfigMatch(LPC_TIM2, &matchCfg);
		
		NVIC_EnableIRQ(TIMER2_IRQn);	//autorisation des interruptions
	}
	
	void startGame() {
		flagJeuEnCours = 1;
		TIM_Cmd(LPC_TIM2, ENABLE);	//d�marrage du timer
	}
	
	void initArrowList() {
		int i;
		bpm = I2C_read_tempo();
		arrowSpeed = bpm/18;
		distanceBetweenArrows = arrowSpeed*900/bpm;
		
		for(i = 0; i < 10; i++) {
			arrowList[i].direction = CENTER;	//initialisation de la direction des fl�ches � 0 pour ne pas essayer d'afficher une fl�che qui n'existe pas
		}
		for(i = 0; i < 10; i++) {	//initialisation du tableau de fl�ches avec les 20 premi�res notes
			I2C_read_mus(noteEnCours, 1);
			//noteEnCours[0] = music[indiceMusique];
			//noteEnCours[1] = music[indiceMusique+1];
			//indiceMusique += 2;
			arrowList[i].yPos = i*(-distanceBetweenArrows);
			if (finFleches) {
				arrowList[i].direction = END;
			} else {
				arrowList[i].direction = noteToDirection(noteEnCours[0]);
			}
			if (noteEnCours[0] == 'y') {	//lorsque le caractère de fin de musique est lu, on arrête de lire les notes
				finFleches = 1;
			} 
		}
	}
	
	void checkJoyInput() {
		if (joyMoved) {	//Gestion du jeu si le joystick n'a pas �t� boug�
			if (joyPos != CENTER){
				if (arrowList[0].yPos >= 200) {
					if (arrowList[0].yPos < 220 && arrowList[0].direction != CENTER) {
						score -= 10;		// malus d'appui trop t�t
					}
					else {
						if (arrowList[0].direction != joyPos) {
							if (arrowList[0].direction != CENTER) {
								score -= 20;	//malus de mauvaise direction
							}
						}
						else if (arrowList[0].yPos >= 260 && arrowList[0].yPos <= 300) {
							score += 60;	//score de pr�cision parfaite
						}
						else {
							score += 20;	//score de pr�cision bonne
						}
					}
					nextArrow();
				}
			}
			joyMoved = 0;
		} else {	//Gestion du jeu si le joystick n'a pas �t� boug� (notes longues)
			if (joyPos != CENTER) {
				if (arrowList[0].yPos >= 240) {
					if (arrowList[0].direction == joyPos) {
						if (arrowList[0].yPos >= 260) {
							score += 60;	//score de pr�cision parfaite
							nextArrow();
						}
					}
				}
			}
		}
	}
		
	void checkNextFrame() {
		uint8_t i;
		if (nextFrameFlag) {
			i = 0;
			while(i < 10) {
				if(arrowList[i].yPos >= 0 && arrowList[i].direction != CENTER) {
					effacer_fleche(arrowList[i].yPos,arrowList[i].direction);			//effacement de la frame pr�c�dente
				}
				if(arrowList[i].yPos < 320) {
					arrowList[i].yPos += arrowSpeed;					//incr�mentation de la position de la fl�che
					if(arrowList[i].yPos >= 0 && arrowList[i].direction != CENTER) {
						if(!musicStarted && arrowList[i].yPos >= 270-(arrowSpeed*4)) {	//On d�marre la musique en m�me temps que la premi�re note doit �tre jou�e
							musicStarted = 1;
							t0_bpm(bpm);
						}
						dessiner_fleche(arrowList[i].yPos,arrowList[i].direction);	//affichage de la fl�che
					}
				} else {
					if (arrowList[0].direction == END) {
						flagJeuEnCours = 0;
					}	else if (arrowList[0].direction != CENTER) {	// pas de malus si pas de note
						score -= 30; 										//malus de note non jou�e
					}
					nextArrow();
				}
				i++;
			}
			//affichage des rep�res et du score
			dessiner_ligne(0,240,240,2,'h',White);
			dessiner_repere(LEFT);
			dessiner_repere(UP);
			dessiner_repere(DOWN);
			dessiner_repere(RIGHT);
			sprintf(scoreString,"Score : %d ", score);
			LCD_write_english_string(70,10,scoreString,White,Black);
			
			nextFrameFlag = 0;
		}
	}
	
	int noteToDirection(char c) {	//Convertit une note en une direction pour les fl�ches
		switch(c) {
					case 'c' : 
						return LEFT; 
					case 'i' : 
						return UP; 
					case 'd' : 
						return DOWN; 
					case 'j' : 
						return RIGHT; 
					case 'e' : 
						return LEFT; 
					case 'f' : 
						return UP; 
					case 'k' : 
						return DOWN; 
					case 'g' : 
						return RIGHT; 
					case 'l' : 
						return LEFT; 
					case 'a' : 
						return UP; 
					case 'h' : 
						return DOWN; 
					case 'b' : 
						return RIGHT; 
					case ' ' :
						return CENTER;
					default : 
						return CENTER;
				}
	}
	
	int jeuEnCours() {
		return flagJeuEnCours;
	}
	
	void resetGame() {
		SendStringToBluetooth(scoreString);
		score = 0;
		musicStarted = 0;
		finFleches = 0;
	}
	
	