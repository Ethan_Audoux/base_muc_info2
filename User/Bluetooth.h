#include "lpc17xx_uart.h"

/*
Initialisation du BT
*/
void InitBluetooth(void);

/*
Permet l'envoie d'un octet de données
*/
void SendIntToBluetooth (uint8_t a);

/*
Permet l'envoie d'une chaine de caractere
*/
void SendStringToBluetooth (char * chaine);

/*
Renvoie la taille d'une chaine de caractere
*/
int lenstr(char *chaine);

/*
Renvoie la taille d'une chaine de caractere sous format uint8_t
*/
int lenuint8_t(uint8_t * chaine);

/*
Fonction de reception par BT avec une interruption
*/
void UART0_IRQHandler(void);

/*
Ajoute l'element au buffer et incremente le curseur d"ecriture de 1
*/
void ajout_buffer(uint8_t elem);

/*
Fonction de reset du buffer
On remet à 0 son curseur
*/
void reset_buffer(void);

/*
On vide le buffer dans la memoire et on le reset
*/
void vider_buffer(void);
