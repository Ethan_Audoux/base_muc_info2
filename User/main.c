
//===========================================================//
// Projet Micro - INFO1 - ENSSAT - S2 2018							 //
//===========================================================//
// File                : Programme de départ
// Hardware Environment: Open1768	
// Build Environment   : Keil µVision
//===========================================================//

#include "lpc17xx_gpio.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_libcfg_default.h"
#include "lpc17xx_timer.h"
#include "touch\ili_lcd_general.h"
#include "touch\lcd_api.h"
#include "affichagelcd.h"
#include "touch\touch_panel.h"
#include "jeu.h"
#include "constantes.h"
#include "Bluetooth.h"
#include "i2c.h"


#include "son.h"
#include "analogJoystick.h"
#include "globaldec.h" // fichier contenant toutes les d�clarations de variables globales
#include <stdio.h>

	
void Init(void); 
void delay1(int);


//===========================================================//
// Function: Main
//===========================================================//
int main(void)

 {	  
	  Init(); // init variable globales et pinsel son et IR
	  lcd_Initializtion(); // init pinsel ecran et init LCD
	  initGame(); //init timer d'affichage/boucle de jeu
	 
	 
		while(1) {
			LCD_write_english_string(30, 140, "Bougez le joystick pour", White, Black);
			LCD_write_english_string(29, 160, "demarrer ou envoyez une", White, Black);
			LCD_write_english_string(38, 180, "musique via Bluetooth", White, Black);
			
			while(lectureDirection() == CENTER){
				delay1(20000);
			}
		 
			
			lcd_clear(Black);	//suppression du menu
			initArrowList();
			
			startGame();	//démarrage du jeu
			
			while(jeuEnCours()) {	//boucle de jeu principale
				checkJoyInput();
				checkNextFrame();
			}
			
			reset_curseur();	//On remet à zéro les curseurs de lecture mémoire
			resetGame();				//et les variables du jeu + envoi du score par bluetooth
			lcd_clear(Black);
		}
	}

	void Init(){
		// pinsel haut parleur
		LPC_PINCON->PINSEL2 &= ~((1<<19)|(1<<18)) ; // on met le pin 1.9 sur la fonction gpio (00) 
		LPC_GPIO1->FIODIR |= 1<<9; 
		LPC_GPIO1->FIOMASK &= ~(1<<9) ; 
		LPC_GPIO1->FIOPIN &= ~(1<<9) ;
		NVIC_EnableIRQ(TIMER1_IRQn);  
		NVIC_EnableIRQ(TIMER0_IRQn);  
		
		// pinsel I2C
		LPC_PINCON->PINSEL1 |= (1<<22)  ;  // pin 0.27 sur la fonction SDA0 (01)
		LPC_PINCON->PINSEL1 &= ~(1<<23)  ;
		LPC_PINCON->PINSEL1 |= (1<<24)  ;  // pin 0.28 sur la fonction SCL0 (01)
		LPC_PINCON->PINSEL1 &= ~(1<<25)  ;
		
		// pinsel bluetooth
		LPC_PINCON->PINSEL0 |= (1<<4)  ;  // pin 0.2 sur la fonction TXD0 (01)
		LPC_PINCON->PINSEL0 &= ~(1<<5)  ;
		LPC_PINCON->PINSEL0 |= (1<<6)  ;  // pin 0.3 sur la fonction RXD0 (01)
		LPC_PINCON->PINSEL0 &= ~(1<<7)  ;
		
		// init bleutooh
		InitBluetooth();
		
		InitI2C();
		

		// pinsel analogJoystick
	
		LPC_PINCON->PINSEL1 |= (1<<16); // pin 1.24 sur AD0.1 (convertisseur analogique num�rique) (01) 
		LPC_PINCON->PINSEL1 &= ~(1<<17);
		LPC_PINCON->PINSEL1 |= (1<<18); // pin 1.25 sur AD0.2 (convertisseur analogique num�rique) (01) 
		LPC_PINCON->PINSEL1 &= ~(1<<19);
		
		init_ADC(); 
		timer0_init() ; 
		timer1_init() ; 
		init_musique();
				
		
		//InitBluetooth(); 
		
		// pinsel joystick
		LPC_PINCON->PINSEL2 &= ~((1<<17)|(1<<16)); // pin 2.8 sur la fonction gpio (00)
		LPC_PINCON->PINSEL2 &= ~((1<<25)|(1<<24)); // pin 2.12 sur la fonction gpio (00)
		LPC_PINCON->PINSEL2 &= ~((1<<27)|(1<<26)); // pin 2.13 sur la fonction gpio (00)
		LPC_PINCON->PINSEL1 &= ~((1<<11)|(1<<10)); // pin 1.20 sur la fonction gpio (00)
		LPC_PINCON->PINSEL1 &= ~((1<<13)|(1<<12)); // pin 1.21 sur la fonction gpio (00)
		
		//Initialisation timers
		
		timer0_init();
		timer1_init();
		
		init_musique();
		
	}
		
		static void delay1 (int cnt)
	{

    cnt <<= DELAY_2N;
    while (cnt--);
	}
		

	
	

//---------------------------------------------------------------------------------------------	
#ifdef  DEBUG
void check_failed(uint8_t *file, uint32_t line) {while(1);}
#endif

