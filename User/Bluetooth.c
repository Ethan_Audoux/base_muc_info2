#include "Bluetooth.h"
#include "core_cm3.h"
#include "i2c.h"


uint8_t IntToUint8 (int a);
UART_CFG_Type myconfigUART;
UART_FIFO_CFG_Type myconfigFIFO;

const int max_buff = 40;
char buffer_BT[max_buff+1];
int empla_buffer;

/*
Initialisation du BT
*/
void InitBluetooth(void) {
	
	UART_ConfigStructInit(&myconfigUART); // Configure une configuration de base pour le BT
	myconfigUART.Baud_rate = 38400; // On met le BaudRate a 38400
	UART_Init(LPC_UART0, &myconfigUART); // ON initialise notre BT avec la configuration

	UART_FIFOConfigStructInit(&myconfigFIFO); // Configuration d'une config pour le FIFO
	UART_FIFOConfig(LPC_UART0, &myconfigFIFO); // Initialisation de notre BT avec la config FIFO
	

	
	UART_TxCmd(LPC_UART0, ENABLE); // Active la transmition par BT
	
	UART_IntConfig(LPC_UART0, UART_INTCFG_RBR, ENABLE); // Autorise les interruption par BT
	NVIC_EnableIRQ(UART0_IRQn); // Autorise la fonction d'interruption
	
	empla_buffer = 0; // Set le curseur du buffer de reception BT au debut
}


/*
Permet l'envoie d'un octet de données
*/
void SendIntToBluetooth (uint8_t a) {
	UART_SendByte(LPC_UART0, a); // Fonction d'envoie
}

/*
Permet l'envoie d'une chaine de caractere
*/
void SendStringToBluetooth (char * chaine) {
	
	int n = lenstr(chaine);
	int i;
	
	uint8_t Nchaine[max_buff];
	
	for (i=1; i<n+1; i++) {
		Nchaine[i] = chaine[i-1];
	}

	// Problème d'affichage sur les 2 premiers caracteres
	Nchaine[0] = ' ';
	
	Nchaine[n+1] = '\n';

		
	
	UART_Send(LPC_UART0, Nchaine, n+2, BLOCKING);  // Fonction d'envoi
}

/*
Renvoie la taille d'une chaine de caractere
*/
int lenstr(char * chaine){
	int res = 0;
	int i = 0;
	
	if (chaine[0] != '\0') {
		 while (chaine[i+1] != '\0') {
			res = res + 1;
			i = i + 1;
		}
		res = res + 1;
	}
	
	return res;	
}

/*
Renvoie la taille d'une chaine de caractere sous format uint8_t
*/
int lenuint8_t(uint8_t * chaine) {
	int res = 0;
	int i = 0;
	
	if (chaine[0] != '\0') {
		 while (chaine[i+1] != '\0') {
			res = res + 1;
			i = i + 1;
		}
		res = res + 1;
	}
	
	return res;	
}
	
/*
Fonction de reception par BT avec une interruption
*/
void UART0_IRQHandler() {
	
	uint8_t chaine;
	
	UART_Receive(LPC_UART0, &chaine, 1, BLOCKING);
	// chaine = UART_ReceiveByte(LPC_UART0); // Reception
	
	
	
	ajout_buffer(chaine); // On ajoute ce qu'on a recu dans un buffer
}


/*
Ajoute l'element au buffer et incremente le curseur d'ecriture de 1
*/
void ajout_buffer(uint8_t elem) {
	char elemChar = (char) elem;
	
	// Si il reste de la place dans le buffer
	if (empla_buffer<max_buff) {
		buffer_BT[empla_buffer] = elemChar;

		empla_buffer++;
		
		// Si on tombe sur l'un de ces caractère, on vide le buffer et on le reset
		if (elemChar == '\n' || elemChar == '\0' || elemChar == '\r' || elemChar == 120 || elemChar == 'x') {
			

	
			vider_buffer();
		}
	}
	// Si il ne reste pas de place dans le buffer
	else {

		vider_buffer();
		buffer_BT[empla_buffer] = elemChar;
		empla_buffer++;
	}	
}


/*
On vide le buffer dans la memoire et on le reset
*/
void vider_buffer() {
	
	I2C_write_mus_glob(buffer_BT, empla_buffer);
	

	
	empla_buffer = 0;
}




