#ifndef CONSTANTES
#define CONSTANTES

#include "lpc17xx_i2c.h"



// definition des fréquences pour chaque note de musique en hz*10e-4
// do
#define note_c1 65406391
#define note_c2 130812782
#define note_c3 261625565
#define note_c4 523251130
#define note_c5 1046502261
// reb
#define note_i1 69295657
#define note_i2 138591315
#define note_i3 277182630
#define note_i4 554365261
#define note_i5 1108730523
// re
#define note_d1 73416191
#define note_d2 146832383
#define note_d3 293664767
#define note_d4 587329535
#define note_d5 1174659071
// mib
#define note_j1 77781745
#define note_j2 155563491
#define note_j3 311126983
#define note_j4 622253967
#define note_j5 1244507934
// mi
#define note_e1 82406889
#define note_e2 164813778
#define note_e3 329627556
#define note_e4 659255113
#define note_e5 1318510227
// fa
#define note_f1 87307057
#define note_f2 174614115
#define note_f3 349228231
#define note_f4 698456462
#define note_f5 1396912925
// solb
#define note_k1 92498605
#define note_k2 184997211
#define note_k3 369994422
#define note_k4 739988845
#define note_k5 1479977690
// sol
#define note_g1 97998858
#define note_g2 195997717
#define note_g3 391995435
#define note_g4 783990871
#define note_g5 1567981743
// lab
#define note_l1 103826174
#define note_l2 207652348
#define note_l3 415304697
#define note_l4 830609395
#define note_l5 1661218790
// la
#define note_a1 110000000
#define note_a2 220000000
#define note_a3 440000000
#define note_a4 880000000
#define note_a5 1760000000
// sib
#define note_h1 116540940
#define note_h2 233081880
#define note_h3 466163761
#define note_h4 932327523
#define note_h5 1864655046
// si
#define note_b1 123470825
#define note_b2 246941650
#define note_b3 493883301
#define note_b4 987766602
#define note_b5 1975533205




typedef struct 
{
	unsigned int PINSEL;
	unsigned int reserve1;
	unsigned int reserve2;
	unsigned int reserve3;
	unsigned int reserve4;
	unsigned int reserve5;
	unsigned int reserve6;
	unsigned int reserve7;
	unsigned int reserve8;	
	unsigned int PINMODE;
}	
PINSEL_Registres ;

//structure utile pour stocker les flèches
typedef struct {
	int yPos;
	uint8_t direction;
} arrow;

// def pinsel 
#define PINSEL0_BASE  (PINSEL_Registres  *)0x4002C000;
#define PINSEL1_BASE  (PINSEL_Registres  *)0x4002C004;
#define PINSEL2_BASE  (PINSEL_Registres  *)0x4002C008;
#define PINSEL3_BASE  (PINSEL_Registres  *)0x4002C00C;
#define PINSEL4_BASE  (PINSEL_Registres  *)0x4002C010;



#endif














