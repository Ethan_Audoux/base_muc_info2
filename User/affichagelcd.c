#include "touch/ili_lcd_general.h"
#include "touch/lcd_api.h"
#include "touch/touch_panel.h"
#include "jeu.h"
#include "globaldec.h"

void dessiner_ligne(unsigned int x, unsigned int y, unsigned int l,unsigned int e, char orientation, unsigned short color)
{
	int i,j;
	if(orientation=='v')
	{
		for(j=y;j<=y+l;j++)
		{
			lcd_SetCursor(x,j);//on place le curseur � la bonne position
			rw_data_prepare();
			for(i=0;i<=e;i++)
			{
				write_data(color);//on trace un point et on passe � la position suivante
			}
		}
	}
	else//orientation='h'
	{
		for(j=y;j<=y+e;j++)
		{
			lcd_SetCursor(x,j);//on place le curseur � la bonne position
			rw_data_prepare();
			for(i=0;i<=l;i++)
			{
				write_data(color);//on trace un point et on passe � la position suivante
			}
		}
	}
}

void dessiner_diagonale(unsigned int x, unsigned int y, unsigned int l, unsigned int e, char orientation, unsigned short color) {
	int i,j;
	int decalage = 0;
	if(orientation == 'd') {
		j = 0;
		while(j < l+e/2) {
			lcd_SetCursor(x+decalage,y+j);	//on place le curseur � la bonne position
			rw_data_prepare();
			i = 0;
			while(i < MIN(e,MIN((2*j)+1,2*(l+(e/2)-j)-1))) {	//cette condition permet de g�rer l'�paisseur variable cr�� par la diagonale
				write_data(color);	//on trace un point et on passe � la position suivante
				i++;
			}
			if(j < e/2) decalage--;
			else decalage++;
			j++;
		}
	}
	if(orientation == 'g') {
		for(j = 0; j <= l+e/2; j++) {
			lcd_SetCursor(x+decalage,y+j);//on place le curseur � la bonne position
			rw_data_prepare();
			for(i = 0; i < MIN(e,MIN((2*j)+1,2*(l+(e/2)-j)+1)); i++) {	//cette condition permet de g�rer l'�paisseur variable cr�� par la diagonale
				write_data(color);//on trace un point et on passe � la position suivante
			}
			if(j < l) decalage--;
			else decalage++;
		}
	}
}

void dessiner_rect(unsigned int x, unsigned int y, unsigned int lng, unsigned int lrg, unsigned int e, unsigned short plein, unsigned short e_color, unsigned short bg_color)
{
	//dessiner fond
	if(plein==1)
	{
		dessiner_ligne(x,y,lng,lrg,'h',bg_color);
	}
	
	//dessiner bordures
	dessiner_ligne(x,y,lng,e,'h',e_color);
	dessiner_ligne(x+lng-e,y,lrg,e,'v',e_color);
	dessiner_ligne(x,y+lrg-e,lng,e,'h',e_color);
	dessiner_ligne(x,y,lrg,e,'v',e_color);
}

//Dessine une fl�che � l'ordonn�e y
void dessiner_fleche(unsigned int y, uint8_t direction) { //!\ si y = -36 �a plante
	switch (direction) {
		case LEFT :
			dessiner_ligne(10,y,40,10,'h',Magenta);
			dessiner_diagonale(30,y-20,20,15,'g',Magenta);
			dessiner_diagonale(17,y+3,20,15,'d',Magenta);
			break;
		case UP :
			dessiner_ligne(85,y-15,40,10,'v',Green);
			dessiner_diagonale(85,y-15,20,15,'g',Green);
			dessiner_diagonale(95,y-15,20,15,'d',Green);
			break;
		case DOWN :
			dessiner_ligne(145,y-15,40,10,'v',Cyan);
			dessiner_diagonale(168,y-2,20,15,'g',Cyan);
			dessiner_diagonale(132,y-2,20,15,'d',Cyan);
			break;
		case RIGHT :
			dessiner_ligne(190,y,40,10,'h',Yellow);
			dessiner_diagonale(210,y-20,20,15,'d',Yellow);
			dessiner_diagonale(223,y+3,20,15,'g',Yellow);
			break;
	}
}

//efface une fl�che � l'ordonn�e y
void effacer_fleche(unsigned int y, uint8_t direction) {
	switch (direction) {
		case LEFT :
			dessiner_ligne(10,y,40,10,'h',Black);
			dessiner_diagonale(30,y-20,20,15,'g',Black);
			dessiner_diagonale(17,y+3,20,15,'d',Black);
			break;
		case UP :
			dessiner_ligne(85,y-15,40,10,'v',Black);
			dessiner_diagonale(85,y-15,20,15,'g',Black);
			dessiner_diagonale(95,y-15,20,15,'d',Black);
			break;
		case DOWN :
			dessiner_ligne(145,y-15,40,10,'v',Black);
			dessiner_diagonale(168,y-2,20,15,'g',Black);
			dessiner_diagonale(132,y-2,20,15,'d',Black);
			break;
		case RIGHT :
			dessiner_ligne(190,y,40,10,'h',Black);
			dessiner_diagonale(210,y-20,20,15,'d',Black);
			dessiner_diagonale(223,y+3,20,15,'g',Black);
			break;
	}
}

//affiche un rep�re de fl�che en bas de l'�cran
void dessiner_repere(uint8_t direction) {
	switch (direction) {
		case LEFT :
			dessiner_diagonale(30,260,7,3,'d',White);
			dessiner_diagonale(30,260,20,3,'g',White);
			dessiner_diagonale(36,266,14,3,'g',White);
			dessiner_ligne(22,280,28,1,'h',White);
			dessiner_ligne(49,280,10,1,'v',White);
			dessiner_ligne(10,280,10,1,'v',White);
			dessiner_ligne(22,289,28,1,'h',White);
			dessiner_diagonale(11,289,20,3,'d',White);
			dessiner_diagonale(23,289,14,3,'d',White);
			dessiner_diagonale(34,302,6,3,'g',White);
			break;
		case UP :
			dessiner_ligne(85,265,10,1,'h',White);
			dessiner_diagonale(85,265,20,3,'g',White);
			dessiner_diagonale(66,284,7,3,'d',White);
			dessiner_diagonale(85,277,14,3,'g',White);
			dessiner_diagonale(95,265,20,3,'d',White);
			dessiner_diagonale(113,283,7,3,'g',White);
			dessiner_diagonale(95,277,14,3,'d',White);
			dessiner_ligne(85,277,28,1,'v',White);
			dessiner_ligne(94,277,28,1,'v',White);
			dessiner_ligne(85,304,10,1,'h',White);
			break;
		case DOWN :
			dessiner_ligne(145,265,10,1,'h',White);
			dessiner_diagonale(126,285,20,3,'d',White);
			dessiner_diagonale(132,278,7,3,'g',White);
			dessiner_diagonale(132,278,14,3,'d',White);
			dessiner_diagonale(174,284,20,3,'g',White);
			dessiner_diagonale(168,278,7,3,'d',White);
			dessiner_diagonale(168,278,14,3,'g',White);
			dessiner_ligne(145,265,28,1,'v',White);
			dessiner_ligne(154,265,28,1,'v',White);
			dessiner_ligne(145,304,10,1,'h',White);
			break;
		case RIGHT :
			dessiner_diagonale(210,260,7,3,'g',White);
			dessiner_diagonale(210,260,20,3,'d',White);
			dessiner_diagonale(204,266,14,3,'d',White);
			dessiner_ligne(190,280,28,1,'h',White);
			dessiner_ligne(190,280,10,1,'v',White);
			dessiner_ligne(229,280,10,1,'v',White);
			dessiner_ligne(190,289,28,1,'h',White);
			dessiner_diagonale(229,289,20,3,'g',White);
			dessiner_diagonale(217,289,14,3,'g',White);
			dessiner_diagonale(204,302,7,3,'d',White);
			break;
	}
}

