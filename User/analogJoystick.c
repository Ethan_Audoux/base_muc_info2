#include "lpc17xx_pinsel.h"
#include "touch\lcd_api.h"
#include "touch\ili_lcd_general.h"
#include "analogJoystick.h"
#include "lpc17xx_adc.h"
#include "globaldec.h"

/*BANCHEMENT ADC 
	fil jaune -> fil rouge -> P0.24  (ADC0.1)  -> valeur en x
	fil blanc -> fil noir -> P0.25	 (ADC0.2)  -> valeur en y 
*/

#define abs(x)(((x) < 0) ? (-(x)) : (x))



int seuil = 900 ; 

void init_ADC(){
	//NVIC_EnableIRQ(ADC_IRQn) ; // plus besoin normalement
	
	
	ADC_Init(LPC_ADC, 2000) ; // taux de conversion 2kHz
	ADC_ChannelCmd(LPC_ADC,ADC_CHANNEL_1, ENABLE); // activation de AD0.1 (x)
	ADC_ChannelCmd(LPC_ADC,ADC_CHANNEL_2, ENABLE); // activation de AD0.2 (y)
	ADC_StartCmd(LPC_ADC, ADC_START_NOW) ; // d�but des conversions 
}

int lectureDirection(){
	int x, y ; 
	
	// lecture x
	ADC_ChannelCmd(LPC_ADC,ADC_CHANNEL_1, ENABLE); // activation de AD0.1 (x) 
	ADC_ChannelCmd(LPC_ADC,ADC_CHANNEL_2, DISABLE); // desactivation de AD0.2	(y) 
	ADC_StartCmd(LPC_ADC, ADC_START_NOW) ; // debut de la convertion
	while(!(ADC_ChannelGetStatus(LPC_ADC, ADC_CHANNEL_1, ADC_DATA_DONE))) ; // on attends la fin de la lecture 
	x = ADC_ChannelGetData(LPC_ADC, ADC_CHANNEL_1) - 3000; // on enl�ve 3000 pour avoir x = 0 si le joystick est au repos 
	
	//lecture y
	ADC_ChannelCmd(LPC_ADC,ADC_CHANNEL_1, DISABLE); // desactivation de AD0.1 (x) 
	ADC_ChannelCmd(LPC_ADC,ADC_CHANNEL_2, ENABLE); // activation de AD0.2	(y) 
	ADC_StartCmd(LPC_ADC, ADC_START_NOW) ; // debut de la convertion
	while(!(ADC_ChannelGetStatus(LPC_ADC, ADC_CHANNEL_2, ADC_DATA_DONE))) ; // on attends la fin de la lecture 
	y = ADC_ChannelGetData(LPC_ADC, ADC_CHANNEL_2) - 3000 ; // on enl�ve 3000 pour avoir y = 0 si le joystick est au repos 
	
	if(abs(x) > abs(y)) {
		if(x<(-seuil)){ 
			//LCD_write_english_string(32,30,"gauche",White,Blue);
			return LEFT ; 
		}
		else if(x>seuil){ 
			//LCD_write_english_string(32,30,"droite",White,Blue);
			return RIGHT ; 
		}
		else {
			//LCD_write_english_string(32,30,"centre",White,Blue);
			return CENTER	;
		}
	}	else {
		if(y<(-seuil)){ 
			//LCD_write_english_string(32,30,"bas   ",White,Blue);
			return DOWN ; 
		}
		else if(y>seuil){
			//LCD_write_english_string(32,30,"haut  ",White,Blue);
			return UP ; 
		}
		else{
			//LCD_write_english_string(32,30,"centre",White,Blue);
			return CENTER	; 
		}
	}
}


/*
void ADC_IRQHandler(){
	x = (LPC_ADC->ADGDR)>>4 & 0x0FFF ; // on d�cale de 4*la piste ADC qu'on veux lire (??)
	y = (LPC_ADC->ADGDR)>>8 & 0x0FFF ;
	
	if(x<(-seuil)) LCD_write_english_string(32,30,"gauche",White,Blue);
	if(x>seuil) LCD_write_english_string(32,30,"droite",White,Blue);
	if(y<(-seuil)) LCD_write_english_string(32,30,"bas",White,Blue);
	if(y>seuil) LCD_write_english_string(32,30,"haut",White,Blue);
	// interuption finie d�s que le registre est lue
}
*/
