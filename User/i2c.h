#include "lpc17xx_i2c.h"

#define mainID 0xA0

/*
Initialisation du module I2C
*/
void InitI2C(void);

/*
Fonction permettant l'ecriture de data d'une longueur length
a l'emplacement 'emplacement' sur la page 'page'
Envoie:
[adresse_escl][page][0][emplacement][data]
*/
void I2C_write(uint8_t * data, uint8_t length, uint8_t emplacement, uint8_t page);

/*
Fonction permettant la lecture de data d'une longueur length
a l'emplacement 'emplacement' sur la page 'page'
Reception
[adresse_escl][page][0][emplacement][adresse_escl][page][1][data]
*/
void I2C_read(uint8_t * data, uint8_t taille, int empla, int page);

/*
lire l'emplacement du curseur d'ecriture
*/
uint8_t I2C_read_empla_cur_wri(void);
/*
lire la page du curseur d'ecriture
*/
uint8_t I2C_read_page_cur_wri(void);

/*
lire l'emplacement du curseur de lecture pour la musique
*/
uint8_t I2C_read_empla_cur_rea_mus(void);
/*
lire la page du curseur de lecture pour la musqiue
*/
uint8_t I2C_read_page_cur_rea_mus(void);

/*
lire l'emplacement du curseur de lecture pour les touches
*/
uint8_t I2C_read_empla_cur_rea_tou(void);
/*
lecture de la page du curseur de lecture pour les touches
*/
uint8_t I2C_read_page_cur_rea_tou(void);

/*
lecture du tempo
*/
uint16_t I2C_read_tempo(void);

/*
ecriture de l'emplacement du curseur d'ecriture
*/
void I2C_write_empla_cur_wri(uint8_t empla);
/*
ecriture de l'emplacement de la page du curseur d'ecriture
*/
void I2C_write_page_cur_wri(uint8_t page);

/*
ecriture de l'emplacement du curseur de lecture pour la musique
*/
void I2C_write_empla_cur_rea_mus(uint8_t empla);
/*
ecriture de la page du curseur de lecture pour la musique
*/
void I2C_write_page_cur_rea_mus(uint8_t page);

/*
ecriture de l'emplacement du curseur de lecture pour les touches
*/
void I2C_write_empla_cur_rea_tou(uint8_t empla);
/*
ecriture de la page du curseur de lecture pour les touches
*/
void I2C_write_page_cur_rea_tou(uint8_t page);

/*
ecriture du tempo
*/
void I2C_write_tempo(uint8_t * tempo);

/*
Fonction global permettant de split la chaine de caractere en chaine
de 45 pour l'ecrire en memoire
*/
void I2C_write_mus_glob(char * str, int len);

/*
Ecriture de la musique en memoire
*/
void I2C_write_mus(char * chaine, int taille);

/*
Lecture de la musique en memoire
*/
void I2C_read_mus(char * data, int mus_tou);

/*
On remet au debut de la musique les 2 curseur de lecture
*/
void reset_curseur(void);

/*
Retourne la page en fonction de l'emplacement depuis le debut de la memoire
*/
uint8_t know_page(int empla);

/*
Renvoie l'adresse de l'esclave en fonction de la page a laquelle on souhiate effectue une action
*/
uint32_t set_adresse(int page);

/*
Renvoie un uint16_t correspondant au tempo de la musique a partir d'un tableau de 3 caracteres
*/
uint16_t set_tempo(char * data);
