# PROJET µC INFO 1 : Un jeu de rythme
- BELLANGER Dorian
- BAZILE Gwenaël 
- AUDOUX Ethan

## L'application 
### Le fonctionnement :
Notre application est un jeu de rythme, c'est-à-dire qu'une musique va être jouée et que l'on va devoir réagir en rythme avec selon les touches demandées.
Pour commencer à jouer, il faut envoyer une chaîne de caractères représentant les notes de la musiques au microC via le module Bluetooth. Cette chaîne de caractères va ensuite être stockée dans la mémoire I2C pour être utilisée plus tard.  On peut également jouer avec la dernière musique mise en mémoire.  
Pour démarrer le jeu, il faut faire un mouvement avec le joystick 
Une fois la musique lancée, des flèches indiquant des directions (haut, bas, gauche, droite) vont défiler à l'écran, le but étant de bouger le joystick dans la direction indiquée lorsqu'une des flèches arrive au repère (voir figure 1). Plus on appuie proche du bon moment, plus on a de points. Les points sont gagnés (ou perdus) suivant les règles suivantes. 

- précision parfaite  : +60
- précision bonne : +20
- appui trop tôt  : -10
- mauvaise direction : -20
- pas d'appui : -30

A la fin de la partie, l'application renvoie le score via le bluetooth.

### Les branchements  : 
- le buzzer doit être branché sur le ETH en haut calé à droite et être aussi connecté en 5V
- l'écran doit être sur le LCD 
- la mémoire I2C doit être branchée sur le I2C0 
- le module bluetooth dois être branché sur le ISP + UART0 calé à droite, avec une broche en l'air
- le joystick analogique doit être branché comme suit : 
  - fil rouge sur 5V
  - fil noir sur GND
  - fil jaune sur P0.24
  - fil blanc sur P0.25 

### Organisation des fichiers 
#### son.c :  
Gestion des timers pour la musique   
  
#### analogJoystick.c  : 
Gestion de l'ADC et fonction de lecture du Joystick analogique   

#### jeu.c
Moteur de jeu (défilement d'écran, match entre les flèches et le joystick...)

#### affichagelcd.c
Gestion de l'affichage sur l'écran 

#### Bluetooth.c
Gestion de l'émission et de la réception de données via le bluetooth

#### i2c.c 
Gestion de la lecture et de l'écriture dans la mémoire i2c

#### globaldec.h
Déclaration des différents états du joystick logique  
Définition des constantes UP, DOWN, CENTER, LEFT, RIGHT symbolisant les différends états des joysticks.

#### constantes.h
Définition des fréquences associées aux notes (générées par un script python)  
Définition de la struct *arrow* symbolisant les flèches défilant à l'écran   



### La minute solfège 
#### la théorie 
Une question s'est posée : comment représenter les musiques par des chaînes de caractères ?  
Pour chaque note on a deux caractères, le premier représente la note, le second l'octave. Les notes définies vont du do2 au do5 et utilisent la correspondance note/caractère suivante  : 
- c <=> do 
- i <=> reb 
- d <=> re
- j <=> mib 
- e <=> mi 
- f <=> fa 
- k <=> solb
- g <=> sol
- l <=> lab
- a <=> la 
- h <=> sib
- b <=> si

Pour le bon formatage des musiques on utilisera le format suivant :  
```bash 
z[tempo][musique]yyx
```
le tempo en entier sur 3 caractères correspond en réalité au bpm divisé par la durée de la note la plus courte
#### Quelques musiques : 

Never gonna give you up (très difficile) : 
```bash 
z450g2a2c3a2e3e3  e3  d3d3d3  g2a2c3a2d3d3  d3  c3c3  b2a2  g2a2c3a2c3c3c3  d3  b3b3b3a3g3  g3  d2d2  c2c2c2c2c2c2yyx
```
Blue *Eiffel 65* (difficile): 
```bash 
z300h4d4g4h4c5f4a4h4h4g4h4d5j5g4d5c5h4d4g4h4c5f4a4h4h4g4h4d5j5g4d5c5h4d4g4h4c5f4a4h4h4g4h4d5j5g4d5c5h4d4g4h4c5f4a4h4h4g4h4d5j5g4d5c5yyx
```
Hollow knight (facile) : 
```bash
z180c4c4c4c4d4j4d4d4d4d4    c4g3l3l3g3f3g3g3g3g3    c4c4c4c4d4j4f4f4f4f4    g4j4d4c4h3h3c4c4c4c4yyx
```



## Organisation de la mémoire I2C ##

Il a fallu organiser la mémoire afin que des données ne se superposent pas.
Ainsi, nous avons adopté l'organisation suivante :

- Page 0 : Informations diverses
  - 0 -> emplacement curseur ecriture
  - 1 -> page curseur ecriture
  - 2 -> emplacement curseur lecture pour la musique
  - 3 -> page curseur lecture pour la musique
  - 4 -> emplacement curseur lecture pour l'affichage
  - 5 -> page curseur lecture pour l'affichage
  - 10 -> centaine du tempo
  - 11 -> dizaine du tempo
  - 12 -> unite du tempo
- Page 1 - ... :
  - 0 : Début de la musique avec la 1ere note
  - ... : musique


## Répartion des tâches  ##

### Dorian
- Écran
- Joystick numérique
- Moteur de jeu
### Ethan 
- Bluetooth
- Mémoire I2C
### Gwenaël 
- Buzzer et musique 
- Joystick analogique 