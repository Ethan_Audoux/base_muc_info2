#ifndef GLOBALDEC
#define GLOBALDEC


#include "constantes.h" // fichier contenant toutes les constantes du projet
#include <stdint.h>
#include "lpc17xx_i2c.h"

//positions du joystick num�rique (plus utilis�)
#define JOY_UP !(LPC_GPIO2->FIOPIN>>13 & 0x0001)
#define JOY_RIGHT !(LPC_GPIO2->FIOPIN>>8 & 0x0001)
#define JOY_DOWN !(LPC_GPIO2->FIOPIN>>12 & 0x0001)
#define JOY_LEFT !(LPC_GPIO1->FIOPIN>>21 & 0x0001)

//directions utilis�es pour les fl�ches
#define LEFT 1
#define UP 2
#define DOWN 3
#define RIGHT 4
#define CENTER 0
#define END 5


#endif
